package ru.jankbyte.spring.jwt.service;

import org.mockito.Spy;
import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.quality.Strictness.LENIENT;

import ru.jankbyte.spring.jwt.properties.JwtTokenProperties;
import ru.jankbyte.spring.jwt.userdetails.JwtUserDetails;

import io.jsonwebtoken.Claims;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.BeforeEach;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Map;
import java.time.Clock;
import java.time.ZoneId;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = LENIENT)
public class JwtServiceTest {
    @Spy
    private Clock clock = Clock.fixed(Instant.now(), ZoneId.of("UTC"));

    private String secret = "czV2OHkvQj9FKEgrTWJRZVNoVm1ZcTN0Nnc5eiRDJkY=";
    private long livingPeriod = 15L;
    private ChronoUnit livingPeriodType = ChronoUnit.SECONDS;

    //@Spy
    //private JwtTokenProperties jwtProps = new JwtTokenProperties(
    //    secret, livingPeriodType, livingPeriod);

    //@InjectMocks
    //private SimpleJwtService service;
}
