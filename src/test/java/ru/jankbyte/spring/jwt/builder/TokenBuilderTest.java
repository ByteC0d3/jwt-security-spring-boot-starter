package ru.jankbyte.spring.jwt.builder;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

import ru.jankbyte.spring.jwt.token.*;

import java.util.Date;
import java.util.Map;

public class TokenBuilderTest {
    @Test
    public void shouldBuildNewToken() {
        Date issued = new Date(45687L);
        Date expiring = new Date(45687765765L);
        String subject = "max";
        Map<String,Object> customClaims = Map.of(
            "test-claim", "hello world",
            "test-claim2", "hello world2"
        );
        JwtToken token = JwtTokenBuilder.builder()
            .withSubject(subject).withExpiring(expiring)
            .withIssued(issued)
            .addClaims(customClaims)
            .build();
        assertThat(token).extracting(
            "subject", "issued", "expiring", "claims")
                .contains(subject, issued, expiring, customClaims);
    }
}