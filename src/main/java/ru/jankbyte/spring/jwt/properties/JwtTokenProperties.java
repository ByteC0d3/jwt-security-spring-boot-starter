package ru.jankbyte.spring.jwt.properties;

import java.time.temporal.ChronoUnit;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "jwt")
public record JwtTokenProperties(String secret) {
}
