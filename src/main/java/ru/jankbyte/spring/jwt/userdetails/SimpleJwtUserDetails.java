package ru.jankbyte.spring.jwt.userdetails;

import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import io.jsonwebtoken.Claims;
import java.util.Map;

public class SimpleJwtUserDetails implements JwtUserDetails {
    private final UserDetails userDetails;
    private final Map<String,Object> claims;

    public SimpleJwtUserDetails(UserDetails userDetails) {
        this(userDetails, null);
    }

    public SimpleJwtUserDetails(UserDetails userDetails,
            Map<String,Object> claims) {
        this.userDetails = userDetails;
        this.claims = claims;
    }

    public UserDetails getOriginalUserDetails() {
        return userDetails;
    }

    @Override
    public Map<String,Object> getClaims() {
        return claims;
    }

    @Override
    public String getPassword() {
        return userDetails.getPassword();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return userDetails.getAuthorities();
    }

    @Override
    public String getUsername() {
        return userDetails.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return userDetails.isAccountNonExpired();
    }

    @Override
    public boolean isAccountNonLocked() {
        return userDetails.isAccountNonLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return userDetails.isCredentialsNonExpired();
    }

    @Override
    public boolean isEnabled() {
        return userDetails.isEnabled();
    }
}
