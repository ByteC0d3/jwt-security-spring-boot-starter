package ru.jankbyte.spring.jwt.userdetails;

import java.util.Map;
import org.springframework.security.core.userdetails.UserDetails;
import io.jsonwebtoken.Claims;

/**
 * The repesentation of user details for JWT.
 */
public interface JwtUserDetails extends UserDetails {
    /**
     * Contains claims (JWT-fields of body, like "subject")
     * @return The map, where key is claim name
     */
    Map<String,Object> getClaims();
}
