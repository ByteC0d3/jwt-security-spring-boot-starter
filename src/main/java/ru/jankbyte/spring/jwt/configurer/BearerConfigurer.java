package ru.jankbyte.spring.jwt.configurer;

import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.config.annotation.web.HttpSecurityBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExceptionHandlingConfigurer;
import org.springframework.security.config.annotation.web.configurers.SessionManagementConfigurer;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

import ru.jankbyte.spring.jwt.filter.*;
import ru.jankbyte.spring.jwt.service.JwtTokenProcessorService;
import ru.jankbyte.spring.jwt.service.JwtTokenDetailsService;
import java.util.LinkedHashMap;

public class BearerConfigurer<B extends HttpSecurityBuilder<B>>
        extends AbstractHttpConfigurer<BearerConfigurer<B>, B> {
    private final JwtTokenProcessorService jwtService;
    private final JwtTokenDetailsService jwtTokenDetailsService;
    private AuthenticationEntryPoint authenticationEntryPoint =
        new BearerAuthenticationEntryPoint();

    public BearerConfigurer(JwtTokenProcessorService jwtService,
            JwtTokenDetailsService jwtTokenDetailsService) {
        this.jwtService = jwtService;
        this.jwtTokenDetailsService = jwtTokenDetailsService;
    }

    public BearerConfigurer authenticationEntryPoint(
            AuthenticationEntryPoint authenticationEntryPoint) {
        this.authenticationEntryPoint = authenticationEntryPoint;
        return this;
    }

    @Override
    public void init(B http) {
        registerDefault(http);
    }

    @Override
    public void configure(B http) {
        BearerAuthenticationFilter filter =
            new BearerAuthenticationFilter(
                jwtService, jwtTokenDetailsService,
                    authenticationEntryPoint);
        filter = postProcess(filter);
        http.addFilterBefore(filter,
            UsernamePasswordAuthenticationFilter.class);
    }

    private void registerDefault(B http) {
        registerStatelessSession(http);
        registerFilterForHandler(http);
    }

    private void registerStatelessSession(B http) {
        SessionManagementConfigurer<B> sessionManagment =
            http.getConfigurer(SessionManagementConfigurer.class);
        if (sessionManagment == null) {
            return;
        }
        sessionManagment.sessionCreationPolicy(STATELESS);
    }

    private void registerFilterForHandler(B http) {
        ExceptionHandlingConfigurer<B> exceptionHandling =
            http.getConfigurer(ExceptionHandlingConfigurer.class);
        if (exceptionHandling == null) {
            return;
        }
        RequestMatcher antMatcher = new AntPathRequestMatcher("/**");
        exceptionHandling.defaultAuthenticationEntryPointFor(
            postProcess(authenticationEntryPoint), antMatcher);
    }
}
