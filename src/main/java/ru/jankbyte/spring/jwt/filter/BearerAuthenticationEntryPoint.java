package ru.jankbyte.spring.jwt.filter;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BearerAuthenticationEntryPoint
        implements AuthenticationEntryPoint {
    private final Logger log = LoggerFactory.getLogger(
        BearerAuthenticationEntryPoint.class);

    @Override
    public void commence(HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException authException) throws IOException {
        log.debug("Sending unauthorized status");
        response.sendError(UNAUTHORIZED.value(),
            UNAUTHORIZED.getReasonPhrase());
    }
}
