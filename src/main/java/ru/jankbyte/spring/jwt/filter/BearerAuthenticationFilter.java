package ru.jankbyte.spring.jwt.filter;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.util.StringUtils;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import io.jsonwebtoken.JwtException;

import ru.jankbyte.spring.jwt.service.JwtTokenProcessorService;
import ru.jankbyte.spring.jwt.service.JwtTokenDetailsService;
import ru.jankbyte.spring.jwt.token.JwtToken;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Filter of JWT for authentication.
 */
public class BearerAuthenticationFilter extends OncePerRequestFilter {
    public static final String AUTHENTICATION_SCHEME_BEARER = "Bearer";

    private final Logger log = LoggerFactory.getLogger(
        BearerAuthenticationFilter.class);

    private final AuthenticationEntryPoint authenticationEntryPoint;
    private final JwtTokenDetailsService jwtTokenDetailsService;
    private final JwtTokenProcessorService jwtService;
    private final UserDetailsChecker checker =
        new AccountStatusUserDetailsChecker();

    public BearerAuthenticationFilter(JwtTokenProcessorService jwtService,
            JwtTokenDetailsService jwtTokenDetailsService,
            AuthenticationEntryPoint authenticationEntryPoint) {
        this.authenticationEntryPoint = authenticationEntryPoint;
        this.jwtService = jwtService;
        this.jwtTokenDetailsService = jwtTokenDetailsService;
        log.debug("JWT filter success initialized by configurer");
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
            HttpServletResponse response, FilterChain filterChain)
                throws IOException, ServletException {
        try {
            String jwt = extractTokenFromRequest(request);
            if (jwt != null) {
                Authentication auth = authenticateRequest(jwt, request);
                onSuccessfulAuthentication(request, response, auth);
            }
        } catch (AuthenticationException ex) {
            log.debug("Authentication exception ({}): {}",
                ex.getClass().getName(), ex.getMessage());
            onUnsuccessfulAuthentication(request, response, ex);
            authenticationEntryPoint.commence(request, response, ex);
            return;
        }
        filterChain.doFilter(request, response);
    }

    public AuthenticationEntryPoint getAuthenticationEntryPoint() {
        return authenticationEntryPoint;
    }

    protected void onSuccessfulAuthentication(HttpServletRequest request,
        HttpServletResponse response, Authentication authResult)
            throws IOException, ServletException {}

    protected void onUnsuccessfulAuthentication(HttpServletRequest request,
        HttpServletResponse response, AuthenticationException authEx)
            throws IOException, ServletException {}

    private Authentication authenticateRequest(String jwt,
            HttpServletRequest request) {
        try {
            Authentication authentication = null;
            JwtToken token = jwtService.createTokenFromString(jwt);
            SecurityContext context = SecurityContextHolder.getContext();
            if (token.getSubject() != null &&
                    context.getAuthentication() == null) {
                UserDetails userDetails =
                    jwtTokenDetailsService.loadUserByJwtToken(token);
                checker.check(userDetails);
                authentication = getToken(userDetails, request);
                context.setAuthentication(authentication);
            }
            return authentication;
        } catch (JwtException ex) {
            log.debug("JwtException ({}): {}",
                ex.getClass().getName(), ex.getMessage());
            throw new BadCredentialsException("Invalid token", ex);
        }
    }

    private Authentication getToken(UserDetails user,
            HttpServletRequest request) {
        UsernamePasswordAuthenticationToken userToken =
            UsernamePasswordAuthenticationToken.authenticated(
                user, null, user.getAuthorities());
        WebAuthenticationDetails authDetails =
            new WebAuthenticationDetailsSource()
                .buildDetails(request);
        userToken.setDetails(authDetails);
        return userToken;
    }

    private String extractTokenFromRequest(HttpServletRequest request) {
        String header = request.getHeader("Authorization");
        if (header == null) {
            return null;
        }
        header = header.trim();
        if (!StringUtils.startsWithIgnoreCase(
                header, AUTHENTICATION_SCHEME_BEARER)) {
            return null;
        }
		if (header.equalsIgnoreCase(AUTHENTICATION_SCHEME_BEARER)) {
			throw new BadCredentialsException(
                "Empty bearer authentication token");
		}
        String token = header.substring(
            AUTHENTICATION_SCHEME_BEARER.length() + 1);
        return token;
    }
}
