package ru.jankbyte.spring.jwt.token;

import java.util.Date;
import java.util.Map;

/**
 * The representation of JSON web token.
 */
public interface JwtToken {
    Map<String,Object> getClaims();
    String getSubject();
    Date getIssued();
    Date getExpiring();
    String getAudience();
    String getIssuer();
}
