package ru.jankbyte.spring.jwt.token;

import java.time.Instant;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;

public final class JwtTokenBuilder {
    private final JwtTokenImpl tokenImpl = new JwtTokenImpl();

    private JwtTokenBuilder() {}

    public static JwtTokenBuilder builder() {
        return new JwtTokenBuilder();
    }

    public JwtTokenBuilder withSubject(String subject) {
        tokenImpl.setSubject(subject);
        return this;
    }

    public JwtTokenBuilder withAudience(String audience) {
        tokenImpl.setAudience(audience);
        return this;
    }

    public JwtTokenBuilder withIssued(Date issued) {
        tokenImpl.setIssued(issued);
        return this;
    }

    public JwtTokenBuilder withIssued(Instant issued) {
        tokenImpl.setIssued(Date.from(issued));
        return this;
    }

    public JwtTokenBuilder withExpiring(Date expiring) {
        tokenImpl.setExpiring(expiring);
        return this;
    }

    public JwtTokenBuilder withIssuer(String issuer) {
        tokenImpl.setIssuer(issuer);
        return this;
    }

    public JwtTokenBuilder withExpiring(Instant expiring) {
        tokenImpl.setExpiring(Date.from(expiring));
        return this;
    }

    public JwtTokenBuilder addClaim(String key, Object value) {
        tokenImpl.getClaims().put(key, value);
        return this;
    }

    public JwtTokenBuilder addClaims(Map<String,Object> claims) {
        tokenImpl.getClaims().putAll(claims);
        return this;
    }

    public JwtToken build() {
        if (tokenImpl.getSubject() == null ||
                tokenImpl.getSubject().isEmpty()) {
            throw new InvalidTokenBuildingException(
                "Token subject is empty");
        }
        return tokenImpl;
    }

    private static final class JwtTokenImpl implements JwtToken {
        private String subject, audience, issuer;
        private Date issued, expiring;
        private final Map<String,Object> claims = new HashMap<>();

        public void setSubject(String subject) {
            this.subject = subject;
        }

        @Override
        public String getSubject() {
            return subject;
        }

        public void setIssued(Date issued) {
            this.issued = issued;
        }

        @Override
        public Date getIssued() {
            return issued;
        }

        public void setIssuer(String issuer) {
            this.issuer = issuer;
        }

        @Override
        public String getIssuer() {
            return issuer;
        }

        public void setExpiring(Date expiring) {
            this.expiring = expiring;
        }

        @Override
        public Date getExpiring() {
            return expiring;
        }

        public void setAudience(String audience) {
            this.audience = audience;
        }
        @Override
        public String getAudience() {
            return audience;
        }

        @Override
        public Map<String,Object> getClaims() {
            return claims;
        }

        @Override
        public String toString() {
            String className = getClass().getSimpleName();
            return "%s[sub=%s, audience=%s, issuer=%s]"
                .formatted(className, subject, audience, issuer);
        }
    }
}