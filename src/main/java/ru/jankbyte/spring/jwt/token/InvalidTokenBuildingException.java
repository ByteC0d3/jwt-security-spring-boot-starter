package ru.jankbyte.spring.jwt.token;

public class InvalidTokenBuildingException extends RuntimeException {
    public InvalidTokenBuildingException(String message) {
        super(message);
    }
}