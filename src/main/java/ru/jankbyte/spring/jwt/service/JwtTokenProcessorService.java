package ru.jankbyte.spring.jwt.service;

import java.util.Map;
import ru.jankbyte.spring.jwt.token.JwtToken;

public interface JwtTokenProcessorService {
    String generateToken(JwtToken jwtToken);
    Map<String,Object> extractClaims(String jwt);
    JwtToken createTokenFromString(String jwt);
}
