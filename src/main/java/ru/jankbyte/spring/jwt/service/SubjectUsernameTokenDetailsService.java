package ru.jankbyte.spring.jwt.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;

import ru.jankbyte.spring.jwt.userdetails.SimpleJwtUserDetails;
import ru.jankbyte.spring.jwt.token.JwtToken;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SubjectUsernameTokenDetailsService
        implements JwtTokenDetailsService {
    private final Logger log = LoggerFactory.getLogger(
        SubjectUsernameTokenDetailsService.class);
    private final UserDetailsService userService;

    public SubjectUsernameTokenDetailsService(
            UserDetailsService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByJwtToken(JwtToken jwtToken) {
        log.debug("Accepting JWT: {}", jwtToken);
        String username = jwtToken.getSubject();
        UserDetails details = userService.loadUserByUsername(username);
        return new SimpleJwtUserDetails(details, jwtToken.getClaims());
    }
}
