package ru.jankbyte.spring.jwt.service;

import org.springframework.security.core.userdetails.UserDetails;
import ru.jankbyte.spring.jwt.token.JwtToken;

/**
 * The service for converting (or loading) user from JWT.
 */
public interface JwtTokenDetailsService {
    /**
     * Load user details by token.
     * @param jwtToken The token with details for loading user
     * @return The user details with token claims
     */
    UserDetails loadUserByJwtToken(JwtToken jwtToken);
}
