package ru.jankbyte.spring.jwt.service;

import ru.jankbyte.spring.jwt.properties.JwtTokenProperties;
import java.security.Key;
import java.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static io.jsonwebtoken.security.Keys.hmacShaKeyFor;

public class SimpleJwtTokenProcessorService
        extends AbstractJwtTokenProcessorService {
    private final Logger log = LoggerFactory.getLogger(
        SimpleJwtTokenProcessorService.class);
    private final Key key;

    public SimpleJwtTokenProcessorService(JwtTokenProperties properties) {
        this.key = generateKey(properties.secret());
        log.info("JWT-processor service success configured");
    }

    private Key generateKey(String secret) {
        byte[] secretBytes = secret.getBytes();
        byte[] decoded = Base64.getDecoder().decode(secretBytes);
        return hmacShaKeyFor(decoded);
    }

    @Override
    protected Key getSigningKey() {
        return key;
    }
}
