package ru.jankbyte.spring.jwt.service;

import io.jsonwebtoken.Jwts;
import ru.jankbyte.spring.jwt.token.*;
import java.security.Key;
import java.util.Map;
import java.util.Date;

public abstract class AbstractJwtTokenProcessorService
        implements JwtTokenProcessorService {
    @Override
    public String generateToken(JwtToken jwtToken) {
        return Jwts.builder().signWith(getSigningKey())
            .setIssuedAt(jwtToken.getIssued())
            .setAudience(jwtToken.getAudience())
            .setIssuer(jwtToken.getIssuer())
            .setExpiration(jwtToken.getExpiring())
            .setSubject(jwtToken.getSubject())
            .addClaims(jwtToken.getClaims())
            .compact();
    }

    @Override
    public JwtToken createTokenFromString(String jwt) {
        Map<String,Object> claims = extractClaims(jwt);
        // TODO: add other fields
        return JwtTokenBuilder.builder()
            .withAudience((String) claims.get("aud"))
            .withSubject((String) claims.get("sub"))
            .withIssuer((String) claims.get("iss"))
            .addClaims(claims).build();
    }

    @Override
    public Map<String,Object> extractClaims(String jwt) {
        return Jwts.parserBuilder()
            .setSigningKey(getSigningKey()).build()
            .parseClaimsJws(jwt).getBody();
    }

    protected abstract Key getSigningKey();
}
