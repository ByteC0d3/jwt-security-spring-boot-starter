package ru.jankbyte.spring.jwt.config;

import ru.jankbyte.spring.jwt.properties.JwtTokenProperties;
import ru.jankbyte.spring.jwt.service.*;

import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetailsService;

@ConditionalOnWebApplication
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(JwtTokenProperties.class)
@ConditionalOnProperty(prefix = "jwt", name = { "secret" })
public class JwtSecurityStarterConfig {
    @Bean
    @ConditionalOnMissingBean(JwtTokenDetailsService.class)
    public JwtTokenDetailsService jwtTokenDetailsService(
            UserDetailsService userDetailsService) {
        return new SubjectUsernameTokenDetailsService(userDetailsService);
    }

    @Bean
    @ConditionalOnMissingBean(JwtTokenProcessorService.class)
    public JwtTokenProcessorService jwtTokenProcessorService(
            JwtTokenProperties properties) {
        return new SimpleJwtTokenProcessorService(properties);
    }
}
